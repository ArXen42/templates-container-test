#include <memory>
#include <iostream>
#include "Container.hpp"
#include "Derived1.hpp"
#include "ContainersCollection.hpp"
#include "Derived2.hpp"

int main()
{
	std::unique_ptr<ContainersCollection> containers(new ContainersCollection());

	{  //Getting container of Derived1 elements and pushing some elements.
		Container<Derived1>* derived1Container = containers->GetContainer<Derived1>();
		derived1Container->items_.push_back(Derived1{42, 1});
		derived1Container->items_.push_back(Derived1{42, 2});

		Container<Derived2>* derived2Container = containers->GetContainer<Derived2>();
		derived2Container->items_.push_back(Derived2{42, 1.1});
		derived2Container->items_.push_back(Derived2{42, 2.2});
	}

	{
		//Getting it again, it is the same container.
		Container<Derived1>* derived1Container = containers->GetContainer<Derived1>();
		for (Derived1& item : derived1Container->items_)
			std::cout << item.baseNum_ << " : " << item.num1_ << std::endl; //Prints two elements pushed earlier

		std::cout << std::endl;

		Container<Derived2>* derived2Container = containers->GetContainer<Derived2>();
		for (Derived2& item : derived2Container->items_)
			std::cout << item.baseNum_ << " : " << item.num2_ << std::endl; //Prints two elements pushed earlier
	}

	//Should also work for any other type.
}