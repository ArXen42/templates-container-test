#pragma once

#include <vector>

template<class T>
class Container
{
public:
	std::vector<T> items_;
};