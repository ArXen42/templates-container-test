#pragma once

#include <evdns.h>

class Base
{
public:
	Base(int64_t baseNum) : baseNum_(baseNum) {}

	int64_t baseNum_;
};