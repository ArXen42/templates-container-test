#pragma once

#include "Base.hpp"

class Derived2 : public Base
{
public:
	Derived2(int64_t baseNum, double num2) : Base(baseNum), num2_(num2) {}

	double num2_;
};