#pragma once

#include <any>
#include <unordered_map>
#include <typeindex>

/// Collection of containers.
class ContainersCollection
{
public:
	/// Returns container of given type contained in collection.
	/// Container is automatically created and pushed into collection if it is the first time the method is called for the given type.
	template<class T>
	Container<T>* GetContainer()
	{
		auto found = GetContainerInternal<T>();

		if (found == nullptr)
			return AddContainer<T>();
		else
			return found;
	}

private:
	template<class T>
	Container<T>* GetContainerInternal() const
	{
		const auto typeIndex = std::type_index{typeid(std::shared_ptr<Container<T>>)};
		auto found = containers_.find(typeIndex);

		if (found == containers_.end())
			return nullptr;
		else
			return std::any_cast<std::shared_ptr<Container<T>>>(containers_.at(typeIndex)).get();
	}

	template<class T>
	Container<T>* AddContainer()
	{
		const auto typeIndex = std::type_index{typeid(std::shared_ptr<Container<T>>)};
		auto newContainer = std::make_shared<Container<T>>();
		containers_.insert(std::pair<std::type_index, std::any>{typeIndex, std::any{newContainer}});

		return newContainer.get();
	}

	std::unordered_map<std::type_index, std::any> containers_;
};