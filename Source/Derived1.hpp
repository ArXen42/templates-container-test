#pragma once

#include "Base.hpp"

class Derived1 : public Base
{
public:
	Derived1(int64_t baseNum, uint32_t num1) : Base(baseNum), num1_(num1) {}

	uint32_t num1_;
};